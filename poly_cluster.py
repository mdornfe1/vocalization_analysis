import numpy as np
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
from math import atan, tan
from collections import OrderedDict
from functools import partial
from global_vars import *

#takes an array of slopes and returns the slopes of the bisectors of these lines sorted along with the original slopes	
def find_bisector(slopes):
	"""The argument slopes is a dict. They keys of the dict are integers which correspond to a mode transition,
	Takes the slopes of a set of straight lines, which pass through the origin, and returns the slopes of the lines that bisect those lines.
	"""
	keys = slopes.keys()
	bisecting_slopes = np.zeros(len(slopes)-1)
	for i, c in enumerate(keys):
		if c == keys[-1]: break
		m1 = slopes[c] 
		m2 = slopes[keys[i+1]]
		bisecting_slopes[i] = tan(atan(m1)/2 + atan(m2)/2)
	
	return bisecting_slopes

#takes slopes of lines, min, and max x values. returns vertices of quadrigon that lie on lines at min and max values. 
def polygon_vertices(slopes, included_clusters, minf = 20e3, maxf = 85e3):
	#vertices = [0 for x in range(len(slopes)+1)]
	vertices = [(n,[]) for n in included_clusters]
	vertices = OrderedDict(vertices)
	
	for i, m1 in enumerate(slopes):
		c = included_clusters[i] 
		if i == 0:
			vertices[c] = np.array([(minf, 0), (minf, m1*minf), (maxf, m1*maxf),
			 (maxf, 0), (minf, 0)])
		else:
			m2 = slopes[i-1]
			vertices[c] = np.array([(minf, minf*m2), (maxf, m2*maxf), 
				(maxf, m1*maxf), (minf, m1*minf), (minf, minf*m2)]			)
		
	m1 = slopes[-1]
	vertices[included_clusters[-1]] = np.array([(minf, m1*minf),
	 (minf, m1*maxf), (maxf, m1*maxf), (minf, m1*minf), (minf, m1*minf)])
			
	return vertices

#creates polygons based on the lines in slopes and checks to see which jumps are in which polygon
def poly_cluster(jumps, vertices): 
	codes = [Path.MOVETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.CLOSEPOLY]

	cluster = -1*np.ones(len(jumps))
	num_clusters = len(vertices)
	for c, v in vertices.iteritems():
		poly = Path(v, codes, closed = True)
		for ij, j in enumerate(jumps):
			if poly.contains_point(j)==1:
				cluster[ij] = c
	
	return cluster

def plot_polygons(vertices, slopes, fig=None, ax=None):
	if ax is None:
		fig, ax = plt.subplots()
		ax.set_xlabel('f1')
		ax.set_ylabel('f2')

	ax.set_title('Clustering jump points using point in polygon algorithm')
	num_clusters=len(vertices)
	codes = [Path.MOVETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.CLOSEPOLY]
	
	#for m in slopes:
	#	ax.plot(x, m*x)
	ax.set_prop_cycle(None)
	color_cycler = ax._get_lines.prop_cycler

	for n,v in vertices.iteritems():
		#ax.plot(v[:,0], v[:,1], 'ro')
		c = color_cycler.next()['color']
		v = v / 1000
		poly = Path(v, codes, closed = True)
		patch = patches.PathPatch(poly, lw=2, color = c, label='n=' + LEGEND[n][0])
		ax.add_patch(patch)

	ax.legend(loc=4, prop={'size':10})

	return fig, ax, ax.patches


def plot_jump_frequencies(jumps, cluster=None):
	plt.style.use('bmh')
	fig, ax = plt.subplots()

	if cluster is None:
		lines = ax.plot(jumps[:,0]/1000, jumps[:,1]/1000, 'o', markersize=2)
		ax.set_title('After jump frequency vs before jump frequency')
	else:
		for n in np.unique(cluster):
			n = int(n)
			idx = np.where(cluster == n)
			x = jumps[idx,0]/1000
			y = jumps[idx,1]/1000
			x.shape = (x.size, )
			y.shape = (y.size, )
			label = 'n=' + LEGEND[n][0]
			ax.plot(x, y, 'o', markersize=3, label=label)
			ax.set_title('Jump points color coded by cluster')

		lines = ax.lines

	ax.set_xlabel('f1 (kHz)')
	ax.set_ylabel('f2 (kHz)')	
	ax.legend(loc=4, prop={'size':10})

	return fig, ax, lines

def plot_lines(slopes, fig, ax):
	f = np.arange(20, 90, 10)
	ax.set_prop_cycle(None)
	for n, s in slopes.iteritems():
		ax.plot(f, s*f, label='n='+ LEGEND[n][0] )

	ax.legend(loc=4, prop={'size':10})
	ax.set_ylim([20, 90])

	return fig, ax, ax.lines

def calc_slopes(gamma, included_clusters):
	slopes = [(n,SLOPES[n](gamma)) for n in included_clusters]
	slopes = OrderedDict(slopes)

	return slopes

def main(jumps, slopes, included_clusters):
	bisecting_slopes = find_bisector(slopes)
	vertices = polygon_vertices(bisecting_slopes, included_clusters)
	cluster = poly_cluster(jumps, vertices)

	return cluster