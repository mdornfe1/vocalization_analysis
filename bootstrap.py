import numpy as np
from math import floor, ceil
import matplotlib.pyplot as plt
import jump_interface
import minimize_cost as mc
from collections import OrderedDict
from matplotlib.ticker import AutoMinorLocator
from IPython import embed

plt.style.use('bmh')

DB_PATH = "./jump.db"
NUM_ITERATIONS = 1000

RAT_CLUSTERS = {'V1':[3,8], 'V2':[3,8], 'V3':[3,8], 'V4':[1,2,3,8,9,10],
		'V5':[3,8], 'V6':[1,2,3,8,9,10], 'V15':[1,3,8,10], 'V17':[3,8], 
		'V18':[3,8], 'V31':[1,3,8,10], 'V32': [3,8]}

def resample(measurements):
	idx = np.random.randint(0, len(measurements), len(measurements))

	return measurements[idx,:]

def calc_ci(estimates, confidence):
	"""Calculates confidence interval for an array of estimates and specified confidence"""
	num_bins = int(np.sqrt(len(estimates)))
	density, edges = np.histogram(estimates, bins=num_bins)
	density = density / float(sum(density))
	distribution = np.cumsum(density)

	a = sum(distribution < confidence)
	a = edges[a]
	b = len(edges) - sum(distribution > (1-confidence))
	b = edges[b]

	return a, b

def resample_estimate(measurements, initial, estimator):
	resampled_measurements = resample(measurements)
	estimate = estimator(resampled_measurements, initial)

	return estimate

def main(measurements, initial, estimator):
	if not hasattr(estimator, '__call__'):
		raise Exception("Variable estimator must be a function.")
	if not isinstance(measurements, np.ndarray):
		raise Exception("Variable measurements must be a numpy array.")     

	estimates = np.zeros((NUM_ITERATIONS, len(initial)))
	error_count = 0
	
	for n in range(NUM_ITERATIONS):
		print(str(n) + '/' + str(NUM_ITERATIONS) + ' iterations completed')
		try:
			resampled_measurements = resample(measurements)
			e = estimator(resampled_measurements, initial)
			estimates[n,:] = e
		except:
			error_count += 1

	confidence = [confidence_interval(m, 0.05) for m in estimates.T]

	return estimates, confidence

def plot_confidence():
	ji = jump_interface.JumpInterface(DB_PATH)
	rats = ['V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V15', 'V17', 'V18']
	labels = ['V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V15', 'V17', 'V18', 
	'edge tone', 'shallow cavity', 'half open pipe', 'deep cavity', 'hole tone']
	whistles = [('edge tone',-0.54), ('shallow cavity',-0.17), ('open pipe', -0.5), ('deep cavity',0.08), ('hole tone',0.25)]
	whistles = OrderedDict(whistles)
	#initials = [[.61,0],[0.38,0],[.36,0],[0.2,0],[0.28,0],[0.43,0],[0.43,0],[.25,0],[0.4,0],[0.35,0],[0.64,0]]
	initials = [-.6 for _ in range(12)]
	initials[3] = -0.8
	initials[5] = -0.5

	fig_t, ax_t = plt.subplots()
	ax_t.set_ylim([0,len(labels)+1])
	ax_t.set_yticks(range(1,len(labels)+1))
	ax_t.set_yticklabels(labels)
	ax_t.set_xlabel('gamma')
	ax_t.set_xlim([-1.0,0.8])
	#ax_t.set_ylabel('rat')
	ax_t.grid(True)
	minorLocator   = AutoMinorLocator()

	i = 1
	for rat, initial in zip(rats, initials):
		print(rat)
		f = './gamma_estimates/'+ rat + '_estimates.npy'
		data = np.load(f)
		jumps = ji.get_jumps(rat)
		included_clusters = RAT_CLUSTERS[rat]
		if rat == 'V6':
			theta = data
			initial = -0.5
		else:
			theta = data[:,0]
		theta_min = mc.minimize_cost(jumps, initial, included_clusters)
		print(theta_min)
		ax_t.plot(1*theta_min, i, color='blue', marker='o', markersize=5)
		#ax_b.plot(b_min, i, 'bo')
		theta_error = calc_ci(theta, confidence=0.025)
		#b_error = confidence_interval(b)
		ax_t.plot(theta_error, [i,i], color='blue', marker='|')
		#ax_b.plot(b_error, [i,i], color='blue', marker='|')
		i+=1

	for w, g in whistles.iteritems():
		ax_t.plot(g, i, color='red', marker='^')
		i+=1

	ax_t.xaxis.set_minor_locator(minorLocator)
	fig_t.tight_layout()

if __name__ == '__main__':
	ji = jump_interface.JumpInterface(DB_PATH)
	
	rats = ['V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V15', 'V17', 'V18', 'V31', 'V32']
	initials = [[-0.4,0],[-0.6,0],[-0.6,0],[-0.8,0],[-0.6,0],[-0.5,0],[-0.5,0],[-0.7,0],[-0.6,0],[-0.6,0],[-0.4,0]]
	for rat, initial in zip(rats, initials):
		jumps = ji.get_jumps(rat)[:,0:2]
		estimates, confidence = main(jumps, initial, minimize_cost.main)
		np.save(rat+'_estimates', estimates)