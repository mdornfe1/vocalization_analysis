#! /usr/bin/env python

import libtfr as tfr
import argparse
import numpy as np
from matplotlib.mlab import find
import matplotlib.pyplot as plt
from scipy.ndimage.measurements import label
from scipy.stats import linregress
import jump_interface
from IPython import embed

#number of time steps in original coordinates in one time step of 
#reassigned coordinate
T_STEP = 30	
NF = 512 # number of frequency points in spectrogram
NFD = NF/2+1 #number of display points
YSKIP = 20 
TSKIP = 100
plt.rc('font', family='serif', size=12)

def calc_mgram(signal):
	mgram = tfr.tfr_spec(signal, NF, T_STEP, NF, fgrid=True)
	mgram[mgram==0] = np.amin(mgram[mgram>0])
	return mgram
	
def low_h(mgram, thresh):
	"""
	returns low entropy time points and the time averaged entropy
	"""
	pt = sum(mgram,0)
	pn = mgram / pt
	valid = -sum(pn*np.log2(pn),0) #entropy as a function of time
	average_h = sum(valid)/len(valid) #time average of H
	valid[valid<thresh] = 1
	valid[valid>thresh] = 0

	return valid, average_h

def plot_mgram(mgram, fs):
	fig, ax = plt.subplots()

	Y_CONV = int(fs/(2*NFD))
	f_disp = np.arange(0, fs/2, YSKIP * Y_CONV) / 1000 
	f_disp = np.array([round(elem, 2) for elem in f_disp])
	t_disp = np.arange(0,(mgram.shape[1]*T_STEP)/fs,T_STEP*TSKIP/fs) 
	t_disp = np.array([round(elem, 2) for elem in t_disp]) * 1000

	ax.set_yticks(np.arange(0,NF,YSKIP))
	ax.set_yticklabels(f_disp)
	ax.set_xticks(np.arange(0,mgram.shape[1],TSKIP))
	ax.set_xticklabels(t_disp)
	ax.grid(True)

	ax.set_xlabel('time (ms)')
	ax.set_ylabel('frequency (kHz)')
	ax.set_title('Reassigned Spectrogram of Rat Ultrasonic Vocalization')

	im = ax.imshow(np.log(mgram), vmin=np.mean(np.log(mgram)), origin='lower')
	im.set_cmap('Purples')

	return fig, ax, im

def plot_curve(s, ts, fs, fig = None, ax = None):
	if ax is None:
		fig, ax = plt.subplots()

	Y_CONV = int(fs/(2*NFD))
	curve_line, = ax.plot(ts*fs/T_STEP, s/Y_CONV, color='orange', label='Fitted Curve')
	ax.set_title('Reassigned Spectrogram and Fitted Curve')

	return fig, ax, curve_line

def plot_jumps(jumps, fs, fig, ax):
	jumps[:,[0,2]] = jumps[:,[2,0]]
	jumps[:,[1,2]] = jumps[:,[2,1]]
	jumps[:,[2,3]] = jumps[:,[3,2]]
	jumps.shape = (jumps.size/2, 2)	
	Y_CONV = int(fs/(2*NFD))	
	jumps_line, = ax.plot(jumps[:, 0]*fs/T_STEP, jumps[:, 1]/Y_CONV, 'o', color = '#98E71A' ,label='Jump Points')
	
	ax.set_title('Reassigned Spectrogram, Fitted Curve, and Jump Points')

	return fig, ax, jumps_line

def plot_jump_frequencies(jumps):
	plt.style.use('bmh')
	fig, ax = plt.subplots()
	line, = ax.plot(jumps[:,0]/1000, jumps[:,1]/1000, 'o', markersize=2)
	ax.set_title('After Jump Frequency Vs Before Jump Frequency')
	ax.set_xlabel('f1 (kHz)')
	ax.set_ylabel('f2 (kHz)')	

	return fig, ax, line

def plot_mean_freq(mgram, t, fs, fig=None, ax=None):
	Y_CONV = int(fs/(2*NFD))
	freq_range = np.arange(int(10e3/Y_CONV), int(100e3/Y_CONV))
	f = np.linspace(0, mgram.shape[0]*Y_CONV, mgram.shape[0])
	m = find(mgram[:,t]==max(mgram[freq_range,t]))[0]
	pt = sum(mgram[m-3:m+4,t])

	fmean = sum(mgram[m-3:m+4, t]*np.arange(m-3,m+4)*Y_CONV/pt)

	if ax is None:
		fig, ax = plt.subplots()

	ax.plot(f, mgram[:,t]/pt, label='t='+str(t*T_STEP/fs*1000)+' ms')
	ax.plot(f[m-3:m+4], mgram[m-3:m+4,t]/pt, 'o', fillstyle='none', color='black')
	#ax.plot(fmean, 0, '+', markersize=20)

	ax.set_xlim(0,80e3)
	ax.set_ylim(-0.01, 0.8)
	ticks = ax.get_xticks()
	ax.set_xticklabels([str(t/1000) for t in ticks])
	ax.legend()
	ax.set_xlabel('f (kHz)')
	ax.set_ylabel('Normalized Power')
	ax.set_title('Illustration of the Curve Fitting Algorithm')

	return fig, ax, ax.lines

def fit_curve(mgram, fs, thresh = 6.9):
	"""
	#YSKIP=number of frequency steps in reassigned coodinates to skip between ticks when plotting mgrams
	#TSKIP=number of time steps in reassigned coordinates to skip between ticks when plotting mgrams
	"""
	#conversion factor between original reassigned frequency space
	Y_CONV = int(fs/(2*NFD))
	#biologically relevant frequency range
	freq_range = np.arange(int(10e3/Y_CONV), int(100e3/Y_CONV))
	 
	tr = np.shape(mgram)[1]
	s = np.zeros(tr)
	ts = np.arange(0, np.shape(mgram)[1]*T_STEP, T_STEP) / fs
	#valid are time points with entropy below thresh. 
	#This function also returns the time average of H.
	valid, average_h = low_h(mgram,thresh) 
	#print valid		
	for t in range(0,tr):
		if valid[t] == 0:
			continue
		
		#m = np.argmax(mgram[freq_range, t], axis=0)
		m = find(mgram[:,t]==max(mgram[freq_range,t]))[0]
		
		try:
			pt = sum(mgram[m-3:m+4,t])
		except:
			continue
		
		s[t] = sum(mgram[m-3:m+4, t]*np.arange(m-3,m+4)*Y_CONV/pt)
	
	return s, ts, valid, average_h
	

def calc_jump_freqs(s, ts, valid):
	"""
	input curves an d valid points returns jump times and frequencies 
	"""
	split_ds = 5e3
	split_dt = 1e-3
	ds = np.diff(s)/split_ds
	dt = np.diff(ts)/split_dt
	#compute distance of jump from origin
	dr = np.sqrt(np.power(ds,2) + np.power(dt,2)) 
	window = np.ones(ts.size-1)	#Discard points within 10% of edge
	window[0:0.1*ts.size] = 0
	window[0.9*ts.size:ts.size] = 0
	dr = window*dr
	l = label(dr<1)	#jump points have dr<1	
	index = find(l[0]==0)	#indices of jump points	
	#remove points whose indices are within 5 places of each other
	remove = np.sort(np.append(find(np.diff(index)<5), 
		find(np.diff(index)<5)+1))	
	index = np.delete(index, remove)
	
	remove = np.array([])    
	for i in range(0,index.size):
		if dr[index[i]]<2:
			remove = np.append(remove,i)
	
	remove.dtype = int
	index = np.delete(index,remove)
	jumps = np.zeros((100,4))
	
	for i in range(0,len(index)):
		#This if statement checks several conditions. If any of them are 
		#satisified the jump point is rejected. 1) Is jump point in high 
		#entropy region, 2) Is the before or the after jump frequency to 
		#high or too low, 3) Is the the difference between before and after 
		#frequencies too high or too low
		if (find(valid[index[i]-15:index[i]+15]==0)!=[] or s[index[i]]<15e3 
		or s[index[i]+1]<15e3 or s[index[i]]>80e3 or s[index[i]+1]>80e3 or 
		abs(s[index[i]]-s[index[i]+1])<7e3 or 
		abs(s[index[i]+1]-s[index[i]])>25e3):
			continue
		else:
			#not sure what this does but leave it there
			if i != len(index)-1 and abs(index[i+1]-index[i])<10:				
				continue
			if i!=0 and abs(index[i-1]-index[i])<10:				
				continue
		jumps[i,:] = np.array([s[index[i]], s[index[i]+1], ts[index[i]],
			ts[index[i]+1]])
	
	jumps = np.delete(jumps,find(jumps==0))
	jumps.shape = (len(jumps)/4, 4)
	
	return jumps
	
#does linear regressions on four points before jump and four points after jump 
#It calculates the new jump frequencies as the values of the regession lines
#at the midpoint of the jump times
def refine_jump(jump, fs, s, ts):
	#before after jump time indices
	i1, i2 = jump[2] * fs / T_STEP, jump[3] * fs / T_STEP 
	m1, b1 = linregress(ts[i1-3:i1+1], s[i1-3:i1+1])[0:2]  
	m2, b2 = linregress(ts[i2:i2+4], s[i2:i2+4])[0:2]	
	t_mid = (jump[3] + jump[2]) / 2
	f1 = b1 + m1 * t_mid # refined before jump freq 
	f2 = b2 + m2 * t_mid # refined after jump freq
	
	jump[0] = f1
	jump[1] = f2
	
	return jump

def make_plot(s, ts, signal, jumps, fs):
	#conversion factor between original reassigned frequency space
	Y_CONV = int(fs/(2*NFD))
	signal = np.reshape(signal,signal.size)
	mgram = calc_mgram(signal)
	plt.imshow(np.log(mgram), vmin=np.mean(np.log(mgram)), origin='lower')
	f_disp = np.arange(0, fs/2,YSKIP*Y_CONV) 
	plt.yticks(np.arange(0,NFD,YSKIP),f_disp)
	t_disp = np.arange(0,(mgram.shape[1]*T_STEP)/fs,T_STEP*TSKIP/fs)
	t_disp = np.array([round(elem, 2) for elem in t_disp])
	plt.xticks(np.arange(0,mgram.shape[1],TSKIP),t_disp)
	plt.plot(ts*fs/T_STEP, s/Y_CONV, color='red', label='Fitted Curve')
	
	if jumps != []:
		jumps.shape=(jumps.size/4,4)		
		plt.plot(jumps[:,2]*fs/T_STEP,jumps[:,0]/Y_CONV,'go',label='Jump Points')
		plt.plot(jumps[:,3]*fs/T_STEP,jumps[:,1]/Y_CONV, 'go')
	
	plt.xlabel('Time (s)')
	plt.ylabel('Frequency (Hz)')
	plt.title('Spectrogram With Curve Fit')

def main(db_path=\
	'/media/matthew/1f84915e-1250-4890-9b74-4bfd746e2e5a/jump.db'):

	ji = jump_interface.JumpInterface(db_path)
	rats = ji.get_rats()[0:11]
	signal_indices = np.array([])
	for r in rats:
		signal_indices = np.append(signal_indices,ji.sget_signal_indices(r))
	signal_indices = np.sort(signal_indices)
	cluster = 0
	quality = 1
	
	for count, signal_index in enumerate(signal_indices):
		print(str(count) + '/' + str(len(signal_indices)))
		signal = ji.get_signal(signal_index)
		fs = ji.get_fs(signal_index)
		mgram = calc_mgram(signal)
		s, ts, valid, average_h = fit_curve(mgram, fs)
		jumps = jump_frequencies(s, ts, valid)
		rat = ji.get_rat(signal_index)
		for jump in jumps:
			jump = refine_jump(jump, fs, s, ts)
			if jump != []:
				ji.insert_jump(rat, jump, int(signal_index), 
					cluster, quality)

		s = s.reshape(len(s), 1)
		ts = ts.reshape(len(ts), 1)
		curve = np.hstack((ts,s))
		curve = curve.reshape((len(s),2))
		ji.insert_curve(rat, curve)  

if __name__ == "__main__":
	#Define command line arguments accepted
	parser = argparse.ArgumentParser()
	#parser.add_argument("-f", "--db_path", help="Data path")
	#parser.add_argument("-r", "--rat", help="ratid")
	args = parser.parse_args()
	
	#main()
	times = [50, 100, 150]
	fig, ax = plt.subplots()
	for t in times:
		plot_mean_freq(mgram, t, fs, fig, ax)

