import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import find
from matplotlib.path import Path
import matplotlib.patches as patches
import sqlite3 as lite
import jump_interface
#import point_browser
from math import atan, tan, sqrt
from IPython import embed
from collections import OrderedDict
from global_vars import *

plt.style.use('bmh')

def calc_slopes(gamma, included_clusters = [1, 2, 3, 8, 9, 10]):
	slopes = [(n,SLOPES[n](gamma)) for n in included_clusters]
	slopes = OrderedDict(slopes)

	return slopes

def plot_jump_frequencies(jumps):
	plt.style.use('bmh')
	fig, ax = plt.subplots()
	line, = ax.plot(jumps[:,0]/1000, jumps[:,1]/1000, 'o', markersize=2)
	ax.set_title('After jump frequency vs before jump frequency')
	ax.set_xlabel('f1 (kHz)')
	ax.set_ylabel('f2 (kHz)')	

	return fig, ax, line

def plot_lines(slopes, fig, ax):
	f = np.arange(20, 90, 10)
	slope_lines = [ax.plot(f, s*f, label='n='+ LEGEND[n][0])[0] for n, s in slopes.iteritems()]
	ax.legend(loc=4, prop={'size':10})
	ax.set_ylim([20, 90])

	return fig, ax, slope_lines


#linear regression forced through origin
def regression(x,y):
	x = np.mat(x)
	y = np.mat(y)
	x.shape=(1,x.size)
	y.shape=(1,y.size)
	m= y * x.T * np.linalg.inv(x*x.T)
	return float(m)

#Error of all the clusters. We're trying to minimize this.
def costFunction(jumps, cluster, slopes):
	cost = 0 
	for c in slopes.keys(): 
		idx = find(cluster==c)
		l = len(jumps[idx,0:2])
		for j in jumps[idx,0:2]:
			x = (j[0] / slopes[c] + j[1]) / (slopes[c] + 1 / slopes[c])
			y = slopes[c] * x
			cost += (((j[0] - x)**2 + (j[1]-y)**2)) / l 
	
	return cost

#takes an array of slopes and returns the slopes of the bisectors of these lines sorted along with the original slopes	
def find_bisector(slopes):
	keys = slopes.keys()
	bisecting_slopes = np.zeros(len(slopes)-1)
	for i, c in enumerate(keys):
		if c == keys[-1]: break
		m1 = slopes[c] 
		m2 = slopes[keys[i+1]]
		bisecting_slopes[i] = tan(atan(m1)/2 + atan(m2)/2)
	
	return bisecting_slopes

#takes slopes of lines, min, and max x values. returns vertices of quadrigon that lie on lines at min and max values. 
def polygon_vertices(slopes, included_clusters=[1, 2, 3, 8, 9, 10], minf = 20e3, maxf = 85e3):
	#vertices = [0 for x in range(len(slopes)+1)]
	vertices = [(n,[]) for n in included_clusters]
	vertices = OrderedDict(vertices)
	
	for i, m1 in enumerate(slopes):
		c = included_clusters[i] 
		if i == 0:
			vertices[c] = np.array([(minf, 0), (minf, m1*minf), (maxf, m1*maxf),
			 (maxf, 0), (minf, 0)])
		else:
			m2 = slopes[i-1]
			vertices[c] = np.array([(minf, minf*m2), (maxf, m2*maxf), 
				(maxf, m1*maxf), (minf, m1*minf), (minf, minf*m2)]			)
		
	m1 = slopes[-1]
	vertices[included_clusters[-1]] = np.array([(minf, m1*minf),
	 (minf, m1*maxf), (maxf, m1*maxf), (minf, m1*minf), (minf, m1*minf)])
			
	return vertices

#creates polygons based on the lines in slopes and checks to see which jumps are in which polygon
def poly_cluster(jumps, vertices, codes = 
	[Path.MOVETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.CLOSEPOLY]):

	cluster = -1*np.ones(len(jumps))
	num_clusters = len(vertices)
	for c, v in vertices.iteritems():
		poly = Path(v, codes, closed = True)
		for ij, j in enumerate(jumps):
			if poly.contains_point(j)==1:
				cluster[ij] = c
	
	return cluster

#shuffle the points around between neighboring clusters to minimize error
def shuffle(jumps, cluster, slopes):
	num_clusters = len(slopes)
	keys = slopes.keys()
	for ij, j in enumerate(jumps):
		#embed()
		c = int(cluster[ij])
		if c==-1: continue
		idx = keys.index(c) 
		e1 = j[1]-slopes[c]*j[0] #original error
		#handle edge cases first 
		if (c==keys[0] and e1<0) or (c==keys[-1] and e1>0): continue
		elif c==keys[0] and e1>0:
			nc = keys[idx+1] #next cluster
			e2 = j[1]-slopes[nc]*j[0]
			if abs(e2)<abs(e1): cluster[ij]=c+1
		elif c==keys[-1] and e1<0:
			pc = keys[idx-1] #prev cluster
			e2 = j[1]-slopes[pc]*j[0]
			if abs(e2)<abs(e1): cluster[ij]=c-1
		else:
			#if above the line and new error is less than original bring it up one cluster
			#if below the line bring it down one
			if e1>0:
				nc = keys[idx+1] #next cluster
				e2 = j[1]-slopes[nc]*j[0]
				if abs(e2)<abs(e1): cluster[ij]=c+1
			if e1<0:
				pc = keys[idx-1] #prev cluster
				e2 = j[1]-slopes[pc]*j[0]
				if abs(e2)<abs(e1): cluster[ij]=c-1
	
	return cluster
		
def plot_jumps(jumps, cluster, slopes):
	num_clusters = len(slopes)
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111, aspect='equal')
	ax1.set_title('After Jump Frequency Vs Before Jump Frequency')
	ax1.set_xlabel('F1')
	ax1.set_ylabel('F2')
	#transitions = [str(n+1)+' to ' +str(n) for n in range(2,6)]
	#transitions = transitions+[str(n)+' to ' +str(n+1) for n in range(5,1,-1)]


	for c in slopes.keys():
		#plot the data set a second time and color code them by cluster
		idx = find(cluster==c)
		if len(idx)==0: continue
		minf = np.amin(jumps[idx,:][:,0])
		maxf = np.amax(jumps[idx,:][:,0])
		f = np.arange(minf, maxf, 100)
		line, = ax1.plot(jumps[idx,:][:,0],jumps[idx,:][:,1], 'o', 
			markersize=2, color = LEGEND[c][1])
		line, = ax1.plot(f, slopes[c]*f, 
			color = LEGEND[c][1], 
			linewidth=1.3, label=LEGEND[c][0])
	ax1.legend()

def plot_polygons(vertices, slopes, fig=None, ax=None):
	if ax is None:
		fig, ax = plt.subplots()
		ax.set_xlabel('f1')
		ax.set_ylabel('f2')

	ax.set_title('Clustering jump points using point in polygon algorithm')
	num_clusters=len(vertices)
	codes = [Path.MOVETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.CLOSEPOLY]
	
	#for m in slopes:
	#	ax.plot(x, m*x)
	
	#embed()
	for n,v in vertices.iteritems():
		#ax.plot(v[:,0], v[:,1], 'ro')
		v = v / 1000
		poly = Path(v, codes, closed = True)
		print(plt.get_cmap('jet')((float(n)+1)/(num_clusters-1)))
		patch = patches.PathPatch(poly, facecolor = LEGEND[n][1], lw=2, label=LEGEND[n][0])
		ax.add_patch(patch)

	ax.legend(loc=4, prop={'size':10})

	return fig, ax, ax.patches	

def insert_jumps(ji, jumps, signal_indices, cluster):
	"""
	Don't use this. It's for inserting values into jump.db, but they're already there.
	"""
	#put the bad jumps on top
	q1 = np.ones((len(jumps)))
	q0 = ji.get_jump_indices_quality(0)
	bad_jumps = ji.get_jumps()[q0]
	bad_signal_indices = ji.get_signal_indices()[q0]
	bad_cluster = -1 * np.ones(len(bad_jumps))
	jumps = np.vstack((jumps, bad_jumps))
	#embed()
	signal_indices = np.concatenate((signal_indices, bad_signal_indices))
	cluster = np.concatenate((cluster, bad_cluster))
	quality = np.concatenate((q1,q0))
	#ji.insert_jumps(jumps, signal_indices, cluster, quality)



def main(jumps, included_clusters = range(NUM_CLUSTERS)):
	#Organize input from db and command line
	#dbPath = args.fileName
	#rat = args.rat
	#jumps = ji.get_jumps(rat)
	#signal_indices = ji.jget_signal_indices(rat)
	#jump_indices = ji.get_jump_indices(rat)
	
	#Brute force through different values of r(theta)
	#Find error of each on
	rationals = np.arange(-1.0001, 1.0001,0.01)
	rTrue = 0
	e = []
	included_clusters = [1, 2, 3, 8, 9, 10]
	for r in rationals:
		print(r)
		slopes = [(n,SLOPES[n](r)) for n in included_clusters]
		slopes = OrderedDict(slopes)
		bisecting_slopes= find_bisector(slopes)
		vertices = polygon_vertices(bisecting_slopes, included_clusters)
		cluster = poly_cluster(jumps[:,0:2], vertices)
		#cluster = shuffle(jumps[:,0:2], cluster, slopes)
		e.append(costFunction(jumps[:,0:2], cluster, slopes))

	#take r with min eror
	e = np.array(e)
	r_min = rationals[np.where(e==min(e))][0]
	print('r_min=' + str(r_min)) 
	slopes = [(n,SLOPES[n](r_min)) for n in included_clusters]
	slopes = OrderedDict(slopes)
	bisecting_slopes= find_bisector(slopes)
	vertices = polygon_vertices(bisecting_slopes, included_clusters)
	cluster = poly_cluster(jumps[:,0:2], vertices)
	#cluster = shuffle(jumps[:,0:2], cluster, slopes)

	#plot_jumps(jumps[:,0:2], cluster, slopes)
	#plot_polygons(vertices, slopes)
	fig_error = plt.figure()
	ax_error = fig_error.add_subplot(111)
	ax_error.plot(rationals, e)
	ax_error.set_xlabel('theta')
	ax_error.set_ylabel('error')
	ax_error.set_title("r = " + str(r_min))
	fig_error.show()
	#print(slopes)
	#plt.show()


	return cluster, slopes, r_min
	"""
	for jump_index in jump_indices:
		ji.update_cluster(jump_index, cluster)
	#point_browser.PointBrowser(ji)
	"""
	

if __name__ == "__main__":
	#Define command line arguments accepted
	#parser = argparse.ArgumentParser()
	#parser.add_argument("-f", "--fileName", help="Data path")
	#parser.add_argument("-r", "--rat", help="Rat Name")
	dbPath = '/media/matthew/1f84915e-1250-4890-9b74-4bfd746e2e5a/jump.db'
	ji = jump_interface.JumpInterface(dbPath)
	#args = parser.parse_args()
	#rat = 'V2'
	jumps = ji.get_jumps(rat)
	signal_indices = ji.jget_signal_indices(rat)
	included_clusters = [1, 3, 6, 8] 
	main(jumps, signal_indices, included_clusters)

