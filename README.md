# Introduction and Acoustics Background

The goal of this project was to analyze recordings of rat ultrasonic vocalizations for the purpose of understanding their acoustic origin. In his 1975 paper, The Rodent Ultrasound Mechanism, Roberts showed the vibration of vocal folds could not account for the acoustic properties of rat ultrasonic vocalizations. He proposed that another acoustic mechanism, called the hole tone, was at the source of these vocalizations. In the hole tone mechanism, air is forced into an axisymmetric jet through a circular aperture. The jet is inherently unstable, causing any small perturbation to grow. The growth of these perturbations will cause an increase in vorticity on the surface of the jet. This increase in vorticity results in a forcing on the air inside a downstream aperture. The vibration in the air of the downstream aperture causes the emission of acoustic radiation. Some of this radiation travels off into the far field, where it can be picked up by recording equipment, but some of it travels backwards, causing further destabilization of the jet. Eventually this feedback mechanism results in a steady state harmonic driving force from the jet, causing acoustic radiation to be emitted at the same frequency. The purpose of this analysis is to show that the hole tone also can not account for the acoustic properties of rat ultrasonic vocalizations.

![Hole tone](images/hole_tone.png?raw=true)

The data consists of acoustic recordings of ultrasonic vocalizations taken from 11 different rats. The analysis consists of extracting features from the spectrograms of these recordings and fitting those features to an acoustic model for the purpose of understanding the mechanism by which the vocalizations are made. The remainder of this document discusses this analysis. 


To run the scripts simply clone the repository and activate the virualenv.

```
git clone https://gitlab.com/mdornfe1/vocalization_analysis.git
cd ./vocalization_analysis
source venv/bin/activate
```

# Working with the Dataset
First let's import the boilerplate modules and setup our namespace. Also we will use the Bayesian Method for Hackers style sheet, because it is quite pretty.

```python
from matplotlib import pyplot as plt
import numpy as np
from jump_interface import JumpInterface
import curve_fit as cf
import poly_cluster as pc
import minimize_cost as mc
import bootstrap
plt.style.use('bmh')
```

The recordings are stored in jump.db. To interact with this data we can use the JumpInterface class. We can get the names of the rats with get_rats method.
	
```python
ji = JumpInterface('./jump.db')
ji.get_rats()
```

This returns an array with the name's of the rats as strings
```python
array([u'V1', u'V15', u'V17', u'V18', u'V2', u'V3', u'V31', u'V32', u'V4',
   u'V5', u'V6'], 
  dtype='<U3')
```
The get_column_names method prints the column layout of the tables in the database.

```python
ji.get_column_names()
```

```python
Table Name: jumps
0 jump_index INTEGER
1 rat 
2 f1 REAL
3 f2 REAL
4 t1 REAL
5 t2 REAL
6 signal_index INTEGER
7 cluster INTEGER
8 quality INTEGER

Table Name: signals
0 signal_index INTEGER
1 rat TEXT
2 Fs REAL
3 signal ARRAY

Table Name: curves
0 signal_index INTEGER
1 rat TEXT
2 curve ARRAY
```

There are several thousand recordings stored in the database in the table signals. Each recording is indexed by a unique integer signal_index. To retrieve a recording from the database use the get_signal method with the appropriate value of signal_index. It will return the recording in the form of a Numpy array. The get_fs method returns the sampling frequency at which that signal was recorded.

```python
signal = ji.get_signal(signal_index=10)
fs = ji.get_fs(signal_index=10)
```

These recordings can most easily be visualized by their spectrogram. The file curve_fit.py contains functions for calculating and plotting the reassigned spectrogram of an audio recording.

```python
mgram = cf.calc_mgram(signal)
fig, ax, im = cf.plot_mgram(mgram, fs)
plt.show()
```

The reassigned spectrogram of this signal is 

![Reassigned spectrogram](images/spectrogram_10.png?raw=true)

# Feature Extraction and Curve Fitting

It can be seen the signal mostly consists of a single frequency modulated tone. Of particular interest to this analysis are the frequency jump points, at which the rat discontinuously changes its frequency of vocalization from one value to another. This analysis is largely concerned with extracting these jump points from the recordings and fitting that data to an acoustic model. To extract the jump points we use the fit_curve function in curve_fit.py to fit a single value curve to the vocalization in spectrogram space. The function works by finding the mean frequency weighted by the energy in each frequency bin, for each time time slice in spectrogram space. The function returns three arrays and one float. The array s is the fitted frequency. The array ts gives the times at which the frequencies are fitted. The array valid is a binary array. A value of 1 signifies the time slice has a signal with entropy below a certain threshold (meaning it is not noisy). A value of 0 signifies the time slice has entropy above the threshold, hence it is not valid. The float average_h gives the average entropy of the recording and is a measure of its overall quality. The function plot_curve plots the fitted curve on top of the spectrogram.

```python
s, ts, valid, average_h = cf.fit_curve(mgram, fs)
fig, ax, curve_line = cf.plot_curve(s, ts, fs, fig, ax)
plt.show()
```

![Reassigned spectrogram and fitted curve](images/spectrogram_curve_10.png?raw=true)

It can be seen that the fit to the recorded vocalization is rather good albeit imperfect. However it is only important that the fit corresponds well at the frequency jump points, which one can see is true. The function calc_jump_freqs extracts the jump frequencies from the fitted curve by calculating the rates of change of the curve from one time slice to the next. The function label from scipy.ndimage.measurements is then used to classify points as a jump points or not based on the instantaneous rate of change. We can then plot the jump points on top of the fitted curve and spectrogram. The array jumps gives the before and after jump frequencies and times. The columns of the array jumps are given by [f_before, f_after, t_before, t_after].

```python
jumps = cf.calc_jump_freqs(s, ts, valid)
fig, ax, jumps_line = cf.plot_jumps(jumps, fs, fig, ax)
plt.show()
```

![Reassigned spectrogram, fitted curve, and jumps](images/spectrogram_curve_jumps_10.png?raw=true)

It can be seen that the jump extraction algorithm is quite accurate. It gives very few false positives. For exploration of the data, there is no need to rerun the jump extraction or curve fitting algorithms, because it is already done! The fitted curves and jumps are stored in the database in the tables curves and jumps respectively. The tables curves and signals both use the integer signal_index as a primary key, but the table jumps uses primary key jump_index. We can use the method get_jump_indices to find values of jump_index associated with a given signal and then select the stored jump frequencies from the table jumps.

```python
curve = ji.get_curve(signal_index=10)
s, ts = np.split(curve, [1], 1)
jump_indices = ji.get_jump_indices(signal_index=10)
jumps = np.array([ji.get_jump(i) for i in jump_indices])
```

# The Clustering Algorithm

We can also retrieve all the jumps made by a given rat. It is useful for the purposes of this analysis to plot the after jump frequency vs. the before jump frequency.

```python
jumps_V6 = ji.get_jumps(rat='V6')[:,0:2]
fig_jumps, ax_jumps, line_jumps = pc.plot_jump_frequencies(jumps_V6)
plt.show()
```

![Before and after jump frequencies](images/plot_jumps.png?raw=true)

From a cursory examination of the data in this plot it can be seen that the points seem to cluster themselves along straight lines. This in accordance with acoustic theory, from which we would expect these jump points to cluster along lines with slopes given by

![Frequency equation](images/freq_equation.png?raw=true) 

The parameter n is an integer which is known as the mode number and has different values for different clusters of points in the plot, the idea being that frequency jumps correspond to transitions between different resonance modes of the system. The parameter γ is constant and is determined by the geometry of the acoustic system. The idea of this analysis is to cluster points in the (f1, f2) plane based on this equation and minimize our cost function over gamma. Getting an estimation for gamma will give us insight into the physical mechanisms and geometry involved in the production of ultrasonic vocalizations. The file cluster_paramter.py contains functions for clustering points and finding an estimation for γ. We can plot the straight lines with slopes given by the above equation over the frequency jump points. Picking a value of gamma at random we see the lines follow the general trend of the data.

```python
included_clusters = [1, 2, 3, 8, 9, 10]
slopes = pc.calc_slopes(gamma = -0.5, included_clusters=included_clusters)
fig_jumps, ax_jumps, line_jumps = pc.plot_jump_frequencies(jumps_V6)
fig_jumps, ax_jumps, line_jumps = pc.plot_lines(slopes, fig_jumps, ax_jumps)
plt.show()
```

![Plot of jump frequencies with clustering lines](images/plot_jumps_lines.png?raw=true) 

Each line in the above plot represents a mode transition. The red line for example represents a frequency jump down from the n=2 to the n=1 mode. The list included_clusters specifies which mode transitions we want to cluster our data into. The global variable LEGEND is a lookup table between the integer entries of included_clusters and the associated mode transition. For example, the entry of 1 in included_clusters means the algorithm is going to cluster some points into the n=2 to n=1 mode transition.

Our goal is to group these point into appropriate mode transition clusters. This is done by assigning a point to the line for which the perpendicular distance between the two objects is minimal. This is most easily done using the point in polygon algorithm. To do this we find the bisectors of the above lines and draw our polygons so that the vertices lie on the bisecting lines. Once we have our polygons drawn we can just assign a point to the polygon in which it lies. 

```python
bisecting_slopes= pc.find_bisector(slopes)
vertices = pc.polygon_vertices(bisecting_slopes, included_clusters)
fig_jumps, ax_jumps, line_jumps = pc.plot_jump_frequencies(jumps_V6)
fig_jumps, ax_jumps, polygons = pc.plot_polygons(vertices, slopes, fig_jumps, ax_jumps)
```

![Plot of jump frequencies with clustering polygons](images/point_in_poly.png?raw=true) 

To find an optimal estimate for the parameter γ we define the cost function to be the sum of the squares of the perpendicular distances of each point to its assigned cluster line.

![Cost function](images/cost_function.png?raw=true) 

Δx<sub>i<sub>c</sub></sub> and Δy<sub>i<sub>c</sub></sub> are horizontal and vertical distances of the i<sub>c</sub>^th point of the cluster c. N<sub>c</sub> is the number of points in cluster c. The script minimize_cost contains functions that minimize the cost function to find an optimal value of gamma. The somewhat redundantly named function minimize_cost returns the optimal value of gamma. The function cost calculate the value of the cost function for a given value of gamma and a selection of jump points. The function plot_cost then plots the cost function and the minimum value of gamma.

```python
gamma_min = mc.minimize_cost(jumps_V6, initial = [-0.5], included_clusters=included_clusters)
cost_min = mc.cost([gamma_min], jumps, included_clusters)
gamma_range = np.arange(-1, 1, 0.01)
cost = np.array([mc.cost([gamma], jumps_V6, included_clusters) for gamma in gamma_range])
fig_cost, ax_cost = plt.subplots()
fig_cost, ax_cost, lines_cost = mc.plot_cost(gamma_range, cost, gamma_min, cost_min)
plt.show()
```

![Cost function plot](images/cost_plot.png?raw=true) 

We can now cluster the frequency jump points with our optimal value of gamma. The function poly_cluster inputs an array of jump points and a dictionary of vertices and outputs an array of integers called cluster. The integers of cluster each represent a distinct mode transition. The global variable LEGEND is a lookup table that gives the correspondence between the entry of cluster and the mode transition. For example if a jump point has a value of cluster equal to 1 that means it is an n=2 to n=1 mode transition.

```python
slopes = pc.calc_slopes(gamma_min, included_clusters)
bisecting_slopes = pc.find_bisector(slopes)
vertices = pc.polygon_vertices(bisecting_slopes, included_clusters)
cluster = pc.poly_cluster(jumps_V6, vertices)
fig_jumps, ax_jumps, line_jumps = pc.plot_jump_frequencies(jumps_V6, cluster)
```

![Clustered jumps](images/clustered_jumps.png?raw=true) 

The value of this result is two-fold. First, it gives us an estimation for the parameter gamma. This gives us and indication of the mechanism which is being used to produce the vocalization. This and how to obtain confidence intervals for these estimates is discussed more in a later section. Second, it gives us a method for classifying ultrasonic vocalizations. Further exploration of the data shows that if we compare vocalization with jump points in the same mode transition cluster we can see they are qualitatively similar. I believe this is indicative of the fact that rats used different resonance modes to communicate different types of information. Although the actual informational content of rat ultrasonic vocalizations is largely unknown there is research to support this. It's been shown that vocalizations in the 22 kHz range are used as a warning call to other rats when a predator is detected. It is also been shown that calls in the 50-85 khZ range are associated with positive mental states and are used in playful and sexual situations. The program point_browser will be discussed more in the next section, but it can be used to interactively view the entire vocalization from which a jump point is extracted.

## Interactive Exploration of the Data
The program point_browser.py can be used to interactively explore the data. To open it simply run it from the terminal (with the virtualenv activated)

```
python point_browser.py
```

The instructions for use are printed in the terminal, but I'll also repeat them here.

Instructions:

Press the up or down key to switch to data from different rats

Select a jump point with the mouse to view the spectrogram of the full recording.

You can also press n to select the next jump point and p to select the previous one.

Use the mode transition selector on the left to choose clusters you want included in the algorithm. 

Press ctrl+shift+c to run the clustering algorithm. It will take less than a minute.

Press c to toggle plotting the fitted curve on top of the spectrogram.

Press a to play a downsampled version of the selected recording through your speakers.

## Inferring the Mechanism Behind Rat Ultrasonic Vocalizations 
By computing confidence intervals on the estimation of gamma for different rats we can infer information about the mechanism by which the recorded vocalizations are produced. The bootstrapping method can be used to calculate these confidence intervals. The script bootstrap.py contains the necessary functions to implement the method. Bootstrapping works by taking measurements from an experiment and randomly sampling those measurements with replacement to get a resampled set of measurements. The number of resampled measurements is equal to the original number of measurements, but some of them may be duplicates since we sampled with replacement. Our measurements are the extracted frequency jumps. The function resample performs the resampling procedure. We can then compute the optimal value of gamma for the resampled measurements and get a slightly different estimate of gamma than for the true set of measurements.

```python
import bootstrap 
jumps_resampled = bootstrap.resample(jumps_V6)
gamma_estimate = mc.minimize_cost(jumps_resampled, initial=-0.5, included_clusters=included_clusters)
```

The idea is then to repeat this resampling and estimating procedure a large number of times to get a histogram of estimates for gamma. The following code performs the resampling and estimating procedure on the jump frequency measurements 1000 times.

```python
jumps_V1 = ji.get_jumps('V1')
resampling_generator = ( bootstrap.resample(jumps_V6) for n in range(1000) )
gamma_estimates = [mc.minimize_cost(jumps_resampled, -0.5, included_clusters) for jumps_resampled in resampling_generator]
```

This is his however very computationally intensive to run so I've included resampled estimates for gamma in this repository. Loading the precomputed estimates and computing their histogram we see they can be interpreted as a probability distribution for the true value of gamma.

```python
gamma_estimates = np.load('./gamma_estimates/V6_estimates.npy')[:,0]
fig_hist, ax_hist = plt.subplots()
num_bins = int(np.sqrt(len(gamma_estimates)))
weights = np.ones_like(gamma_estimates)/len(gamma_estimates)
ax_hist.hist(gamma_estimates, num_bins, weights=weights)
ax_hist.set_xlabel('gamma')
ax_hist.set_ylabel('probability')
ax_hist.set_title('Histogram of Gamma Estimates')
plt.show()
```

![Gamma histogram](images/gamma_histogram.png?raw=true) 

With the interpretation of the entries of gamma_estimates as being drawn from this probability distribution we can compute confidence intervals for the estimation of gamma. To compute the 95% confidence interval of this estimate we use the function calc_ci with the key word argument confidence=0.025. The function returns the endpoints of the confidence interval. The function plot_confidence calculates and plots confidence intervals on gamma for all of the rats and compares them to known values of gamma for different acoustic systems. 

```python
a, b = bootstrap.calc_ci(gamma_estimates, confidence=0.025)
bootstrap.plot_confidence()
```

![Gamma confidence](images/confidence_intervals.png?raw=true) 

In this plot it can be seen that the values of gamma for the different rats seem to group around -0.6. Some rats give a slightly different value for gamma. I believe this is because there was data for these rats. V1 for example only seem to vocalize in the n=3 and n=4 modes. I believe this may have affected the estimation of gamma for this rat. Nevertheless the value of gamma for the hole tone is well outside the confidence intervals for gamma of the different rats. Thus, it is very unlikely that this is the source of their vocalizations. Also shown in this plot are known values of gamma for different acoustic mechanisms. Based on this plot, the edge tone is a candidate for the true mechanism. However, the anatomy of the rat vocal tract makes this an unlikely source. The half open pipe is anatomically consistent with the rat vocal tract, but it still lies outside most of the calculated confidence intervals. Further theoretical work needs to be done on modeling the acoustics of the rat vocal tract. It is my view that the true mechanism is a combination of the hole tone and the half open pipe, where vorticity growth of an axisymmetric jet drives the resonance modes of a half open pipe.