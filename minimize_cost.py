import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import find
import sqlite3 as lite
import jump_interface
from IPython import embed
from collections import OrderedDict
from scipy.optimize import fmin_ncg
import poly_cluster
from global_vars import *


def dcost(x, *args):
	""" Derivative of cost function"""
	theta = x[0]
	b = 0

	jumps = args[0]
	included_clusters = args[1]

	slopes = OrderedDict([(n,SLOPES[n](theta)) for n in included_clusters])
	dslopes = OrderedDict([(n, DSLOPES[n](theta)) for n in included_clusters])
	
	cluster = poly_cluster.main(jumps, slopes, included_clusters)

	dcost_theta = 0
	dcost_b = 0
	for c in slopes.keys(): 
		idx = find(cluster==c)
		l = len(jumps[idx,:])
		for j in jumps[idx,:]:
			x = (j[0] / slopes[c] + j[1] - b) / (slopes[c] + 1 / slopes[c])
			y = slopes[c] * x + b 
			
			dx_theta = ((slopes[c]**2 + 1) * (j[1] - b) * dslopes[c] - 
				2 * (j[0] + (j[1] - b) * slopes[c]) * slopes[c] * dslopes[c]) / (slopes[c]**2 + 1)**2
			
			dy_theta = ((slopes[c]**2 + 1) * (j[0] * dslopes[c] + 2 * (j[1] - b) * slopes[c] * dslopes[c]) 
				- 2 * (j[0] * slopes[c] + (j[1] - b) * slopes[c]**2) * slopes[c] * dslopes[c]) / (slopes[c]**2 + 1)**2
			#dx_b = -slopes[c] / (slopes[c]**2 + 1)
			#dy_b = -slopes[c]**2 / (slopes[c]**2 + 1)

			dcost_theta += (2 * (j[0] - x) * dx_theta 
				+ 2 * ((j[1] - b) - y) * dy_theta) / l

			#dcost_b += (2 * (j[0] - x) * dx_b 
				#+ 2 * ((j[1] - b) - y) * dy_b) / l

	return dcost_theta

def cost(x, *args):
	"""Cost function. We're trying to minimize this."""
	theta = x[0]
	b = 0

	jumps = args[0]
	included_clusters = args[1]

	slopes = OrderedDict([(n,SLOPES[n](theta)) for n in included_clusters])

	cluster = poly_cluster.main(jumps, slopes, included_clusters)

	cost = 0 
	for c in slopes.keys(): 
		idx = find(cluster==c)
		l = len(jumps[idx,:])
		for j in jumps[idx,:]:
			x = (j[0] / slopes[c] + j[1] - b) / (slopes[c] + 1 / slopes[c])
			y = slopes[c] * x + b 
			cost += (((j[0] - x)**2 + (j[1]-y)**2)) / l 

	return cost

def minimize_cost(jumps, initial, included_clusters = [1, 2, 9, 10]):
	
	mins, trajectory = fmin_ncg(f=cost, x0=initial, fprime=dcost,
	 args=(jumps, included_clusters), avextol=1e-5, disp=0, callback=None, 
	 retall=True)

	theta_min = mins[0]
	trajectory = np.array(trajectory)
	theta = np.arange(-1, 1, 0.1)
	b = 0
	theta_cost =[cost([t], jumps, included_clusters) for t in theta]
	
	return theta_min

def calc_cost(gamma_range, jumps):
	return np.array([cost(gamma, jumps) for gamma in gamma_range])

def plot_cost(gamma_range, cost, gamma_min, cost_min, fig=None, ax=None):
	if ax is None:
		fig, ax = plt.subplots()

	ax.plot(gamma_range, cost, label='cost function')
	ax.plot(gamma_min, cost_min, 'o', label='gamma_min=' + str(round(gamma_min, 3) ))
	ax.set_xlabel('gamma')
	ax.set_title('Plot of cost function and local minimum')
	ax.legend(loc=0)

	return fig, ax, ax.lines

if __name__ == "__main__":
	dbPath = './jump.db'
	ji = jump_interface.JumpInterface(dbPath)
	"""
	rat_clusters={'V1':[3,6], 'V2':[3,6], 'V3':[3,6], 'V4':[1,2,3,4,5,6,7,8],
		'V5':[3,6], 'V6':[1,2,3,4,5,6,7,8], 'V15':[1,3,6,8], 'V17':[3,6], 
		'V18':[3,6], 'V31':[1,3,6,8], 'V32': [3,6]}
	"""
	rat_clusters={'V6':[1,2,3,4,5,6,7,8]}
	for rat, included_clusters in rat_clusters.iteritems():
		jumps = ji.get_jumps(rat)[:,0:2]
		main(jumps, included_clusters)
