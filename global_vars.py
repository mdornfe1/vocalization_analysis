from collections import OrderedDict
from functools import partial
import brewer2mpl as brew

def slope(n1, n2, r):
	return float(n2-r) / (n1-r)

def dslope(n1, n2, theta):
	return float(n1-n2) / (n1 - theta)**2

SLOPES = OrderedDict([(0, partial(slope,1,0)), (1, partial(slope,2,1)), (2, partial(slope,3,2)), (3, partial(slope,4,3)), 
	(4, partial(slope,5,4)), (5,partial(slope,6,5)), (6, partial(slope,5,6)), (7,partial(slope,4,5)), 
(8,partial(slope,3,4)), (9,partial(slope,2,3)), (10, partial(slope,1,2)), (11, partial(slope,0,1))])

DSLOPES = OrderedDict([(0, partial(dslope,1,0)), (1, partial(dslope,2,1)), 
	(2, partial(dslope,3,2)), (3, partial(dslope,4,3)), (4, partial(dslope,5,4)), 
	(5,partial(dslope,6,5)), (6, partial(dslope,5,6)), (7,partial(dslope,4,5)), 
	(8,partial(dslope,3,4)), (9,partial(dslope,2,3)), (10, partial(dslope,1,2)), 
	(11, partial(dslope,0,1))])

COLOR_MAP = (brew.get_map('Set3', 'qualitative', 10).mpl_colors +
	brew.get_map('Accent', 'qualitative', 6).mpl_colors)

LEGEND = OrderedDict([(-1,("unclustered", COLOR_MAP[1])), (0,("1 to 0", COLOR_MAP[5])), 
	(1,("2 to 1", COLOR_MAP[0])), (2,("3 to 2", COLOR_MAP[2])), (3,("4 to 3", COLOR_MAP[3])), 
	(4,("5 to 4", COLOR_MAP[4])), (5,("6 to 5", COLOR_MAP[5])), 
	(6,("5 to 6", COLOR_MAP[6])), (7,("4 to 5", COLOR_MAP[7])), 
	(8,("3 to 4", COLOR_MAP[8])), (9,("2 to 3", COLOR_MAP[9])), 
	(10,("1 to 2", COLOR_MAP[10])), (11,("0 to 1", COLOR_MAP[6]))])

CLUSTERS = OrderedDict([(0, {'n1':1, 'n2':0}), (1, {'n1':2, 'n2':1}), (2, {'n1':3, 'n2':2}), 
	(3, {'n1':4, 'n2':3}), (4, {'n1':5, 'n2':4}), (5, {'n1':6, 'n2':5}), 
	(6, {'n1':5, 'n2':6}), (7, {'n1':4, 'n2':5}), (8, {'n1':3, 'n2':4}), 
	(9, {'n1':2, 'n2':3}), (10, {'n1':1, 'n2':2}), (11, {'n1':0, 'n2':1}) ])

REVERSE_LEGEND = OrderedDict([("unclustered", -1), ("1 to 0", 0), ("2 to 1", 1), ("3 to 2", 2), 
	("4 to 3", 3), ("5 to 4", 4), ("6 to 5", 5), ("5 to 6", 6), ("4 to 5", 7), ("3 to 4", 8), ("2 to 3", 9),
	("1 to 2", 10), ("0 to 1", 11)])

NUM_CLUSTERS = len(LEGEND) - 1

INSTRUCTIONS = ('Instructions:\n \n' +
	'Press the up or down key to switch to data from different rats. \n \n' + 
	'Select a jump point with the mouse to view the spectrogram of the full recording. \n \n' +
	'Use the mode transition selector on the left to choose clusters you want included in the algorithm. \n' +
	'Press ctrl+shift+c to run the clustering algorithm. It will take less than a minute. \n \n' + 
	'Press c to toggle plotting the fitted curve on top of the spectrogram \n \n' +
	'Press a to play a downsampled version of the selected recording through your speakers.'
	)