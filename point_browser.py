import jump_interface
import cluster_parameter
import minimize_cost as mc
import poly_cluster
from global_vars import *
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons, LassoSelector, RadioButtons 
from matplotlib.path import Path
import numpy as np
import libtfr as tfr
from IPython import embed
import brewer2mpl as brew
from operator import not_
from collections import OrderedDict
from scikits.audiolab import play
import sys
import contextlib
import sys
import cStringIO

@contextlib.contextmanager
def nostdout():
    save_stdout = sys.stdout
    sys.stdout = cStringIO.StringIO()
    yield
    sys.stdout = save_stdout


plt.style.use('bmh')

"""
Class for investigating jumps. It plots the after vs before jump freqs.You 
can click on them to display the reassinged spectrogram in another window.
"""

def format_e(n):
	a = '%e' % n
	significand = a.split('e')[0].rstrip('0').rstrip('.')
	mantessa = a.split('e')[1]
	if len(significand) == 1:
		significand = significand + '.0'
	return  significand + 'e' + mantessa 

class PointBrowser:
	def __init__(self, rat, ji):
		self.lines_visible = False
		self.ji = ji
		self.rats = ji.get_rats()
		if rat not in self.rats:
			raise Exception("That rat doesn't exist.")

		self.clusters = OrderedDict()
		self.included_clusters = OrderedDict()
		self.jumps = OrderedDict()
		self.signal_indices = OrderedDict()
		self.slopes = OrderedDict()
		for r in self.rats:
			l = len(ji.get_jumps(r))
			self.clusters[r] = -1 * np.ones(l)
			self.included_clusters[r] = [1, 2, 9, 10] 
			self.jumps[r] = ji.get_jumps(r)
			self.signal_indices[r] = ji.jget_signal_indices(r)
			self.slopes[r] = []

		self.rat_index = np.where(self.rats == rat)[0][0]
		self.rat = rat
		self.setup_figures()
		print(INSTRUCTIONS)
		plt.show()

		
	def change_rat(self, event):
		if event.key not in ('up', 'down'): return
		if event.key == 'up':
			self.rat_index = (self.rat_index + 1)%len(self.rats)
		else: 
			self.rat_index = (self.rat_index - 1)%len(self.rats)
		
		self.rat = self.rats[self.rat_index]
		self.setup_figures()

	
	def reload_defaults(self, event):
		if event.key != "ctrl+" + 'R': return
		self.jumps[self.rat] = ji.get_jumps(self.rat)
		self.signal_indices[self.rat] = ji.jget_signal_indices(self.rat)
		self.clusters[self.rat] = -1 * np.ones(len(self.jumps[self.rat])) 

		self.setup_figures()

	def setup_figures(self):
		self.jump_index = 0
		if 'fig_jumps' not in self.__dict__.keys():
			#create figures
			self.fig_jumps = plt.figure()
			self.fig_mgram = plt.figure()
			self.fig_cost, self.ax_cost = plt.subplots()

			#Connect click and press events to callback function
			self.fig_jumps.canvas.mpl_connect('key_press_event', self.reload_defaults)
			self.fig_jumps.canvas.mpl_connect('key_press_event', self.on_press)
			self.fig_jumps.canvas.mpl_connect('key_press_event', self.change_rat)
			self.fig_jumps.canvas.mpl_connect('key_press_event', self.cluster_data)
			self.fig_mgram.canvas.mpl_connect('key_press_event', self.on_press)
			self.picker = self.fig_jumps.canvas.mpl_connect('pick_event', self.on_pick)
			
			#create axes
			self.ax_jumps = self.fig_jumps.add_subplot(111, aspect='equal')
			self.ax_mgram = self.fig_mgram.add_subplot(111)

			#create cluster checker
			rect = [0.02, 0.7, 0.1, 0.25] #l,b,w,h
			self.ax_checker = self.fig_jumps.add_axes(rect)

		else:
			self.ax_jumps.cla()
			self.ax_mgram.cla()
			self.ax_checker.cla()

		#update cluster checker
		self.ax_checker.set_title('Mode \n           Transition Selections')
		labels = tuple(LEGEND[n][0] for n in range(NUM_CLUSTERS))
		actives = tuple(n in self.included_clusters[self.rat] 
			for n in range(NUM_CLUSTERS))
		self.check = CheckButtons(self.ax_checker, labels, actives)
		self.check.on_clicked(self.on_check) 

		#set axes properties
		title = (str(self.rat) + " After Jump Frequency Vs Before Jump"  
			" Frequency")
		self.ax_jumps.set_title(title)
		self.ax_jumps.set_xlabel('f1')
		self.ax_jumps.set_ylabel('f2')

		#plot jumps invisibly for click selector
		line,=self.ax_jumps.plot(self.jumps[self.rat][:,0], 
			self.jumps[self.rat][:,1],'o', 
			markersize=2, visible = False , picker=5)

		f = np.arange(20e3, 80e3, 100)
		
		#plot unclustered jumps first
		self.ax_jumps.set_prop_cycle(None)
		n = -1
		idx = np.where(self.clusters[self.rat]==n)[0]
		line, = self.ax_jumps.plot(self.jumps[self.rat][idx][:,0], 
				self.jumps[self.rat][idx][:,1],'o',markersize=2.3, 
				 label = LEGEND[n][0])
		#plot clustered jumps
		self.ax_jumps.set_prop_cycle(None)
		color_cycler = self.ax_jumps._get_lines.prop_cycler
		for c in self.included_clusters[self.rat]:

			idx = np.where(self.clusters[self.rat]==c)[0]

			color = color_cycler.next()['color']
			line, = self.ax_jumps.plot(self.jumps[self.rat][idx][:,0], 
				self.jumps[self.rat][idx][:,1], 'o', markersize=2.3, 
				color=color, label=LEGEND[c][0])

			if len(self.slopes[self.rat]) != 0:
				line, = self.ax_jumps.plot(f, f*self.slopes[self.rat][c], color=color)


		#create legend self.ax_jumps.legend()
		handles, labels = self.ax_jumps.get_legend_handles_labels()
		rect = [0.9, 0.8, 0.1, 0.15] #l,b,w,h
		self.ax_legend = self.fig_jumps.add_axes(rect)
		self.ax_legend.get_xaxis().set_visible(False)
		self.ax_legend.get_yaxis().set_visible(False)
		self.ax_legend.set_axis_bgcolor((0.75, 0.75, 0.75, 1.0))
		self.ax_legend.set_frame_on(False)
		self.ax_legend.legend(handles, labels, markerscale=3)

		#plot red dot
		self.selected,= self.ax_jumps.plot([self.jumps[self.rat][0,0]], 
			[self.jumps[self.rat][0,1]], 'o', color='red', visible=False)
		
		#set axis limits
		self.ax_jumps.set_xlim(f[0], f[-1])
		self.ax_jumps.set_ylim(f[0], f[-1])

		
		#draw stuff
		self.fig_jumps.canvas.draw()
		self.fig_mgram.canvas.draw()
		self.fig_cost.canvas.draw()

	def selection_mode(self, label):
		labels = ["Point Selector", "Exclusive Lasso", "Inclusive Lasso"]
		if label == labels[0]:
			if 'lasso' in self.__dict__.keys():
				self.lasso.disconnect_events()
			self.fig_jumps.canvas.mpl_connect('pick_event', self.on_pick)
		elif label == labels[1]:
			self.lasso = LassoSelector(self.ax_jumps, self.exclusive_on_lasso)
			self.fig_jumps.canvas.mpl_disconnect(self.picker)
		elif label == labels[2]:
			self.lasso = LassoSelector(self.ax_jumps, self.inclusive_on_lasso)
			self.fig_jumps.canvas.mpl_disconnect(self.picker) 

	def exclusive_on_lasso(self, verts):
		p = Path(verts)
		ind = p.contains_points(self.jumps[self.rat][:,0:2])
		ind = map(not_, ind)
		ind = np.where(ind)[0]
		self.jumps[self.rat] = self.jumps[self.rat][ind,:]
		self.signal_indices[self.rat] = self.signal_indices[self.rat][ind]
		self.clusters[self.rat] = self.clusters[self.rat][ind]
		self.setup_figures()

	def inclusive_on_lasso(self, verts):
		p = Path(verts)
		ind = p.contains_points(self.jumps[self.rat][:,0:2])
		ind = np.where(ind)[0]
		self.jumps[self.rat] = self.jumps[self.rat][ind,:]
		self.signal_indices[self.rat] = self.signal_indices[self.rat][ind]
		self.clusters[self.rat] = self.clusters[self.rat][ind]
		self.setup_figures()

	def on_check(self, label):
		cluster = REVERSE_LEGEND[label]
		if cluster in self.included_clusters[self.rat]:
			self.included_clusters[self.rat].remove(cluster)
		else:
			self.included_clusters[self.rat].insert(cluster, cluster)

		self.included_clusters[self.rat].sort()	

		self.fig_jumps.canvas.draw()
	
	def cluster_data(self, event):
		if event.key != "ctrl+" + 'C': return
		jumps = self.jumps[self.rat]
		included_clusters = self.included_clusters[self.rat]
		
		gamma_min = mc.minimize_cost(jumps, [-0.5], included_clusters)
		
		slopes = poly_cluster.calc_slopes(gamma_min, included_clusters)

		self.slopes[self.rat] = slopes

		self.clusters[self.rat] = poly_cluster.main(jumps, slopes, included_clusters)
		
		cost_min = mc.cost([gamma_min], jumps, included_clusters)
		gamma_range = np.arange(-1, 1, 0.01)
		cost = np.array([mc.cost([gamma], jumps, included_clusters) for gamma in gamma_range])
		self.ax_cost.cla()
		self.fig_cost, self.ax_cost, lines_cost = mc.plot_cost(gamma_range, cost, gamma_min, 
			cost_min, self.fig_cost, self.ax_cost)
		self.setup_figures()
		
		for n, m in zip(self.included_clusters[self.rat], self.slopes[self.rat]):
			label = LEGEND[n][0]

	def play_sound(self):
		with nostdout():
			play(self.signal, fs=20e3)

	def on_press(self, event):
		#Show next plot if your press, previous plot if your press p
		if self.jump_index is None: return
		if event.key not in ('n', 'p', 'a', 'c'): return
		if event.key == 'a':
			self.play_sound()
			return 
		if event.key == 'c':
			self.lines_visible = not self.lines_visible  
			for line in self.ax_mgram.lines:
				line.set_visible(self.lines_visible)
			self.fig_mgram.canvas.draw()
			return

		if event.key=='n': inc = 1
		else:  inc = -1
		
		#increment jump index  
		self.jump_index += inc
		self.jump_index = int(np.clip(self.jump_index, 0, len(self.jumps[self.rat])-1))
		
		#draw yellow circle on graph
		self.selected.set_data(self.jumps[self.rat][self.jump_index,0], 
			self.jumps[self.rat][self.jump_index,1])
		self.selected.set_visible(True)
		self.fig_jumps.canvas.draw()

		#update signal data and graph mgram
		self.get_signal_data()
		self.graph_mgram()
	
	def on_pick(self, event):		
		#event.ind is the list of all jump points within a certain
		#distance of selected point
		N = len(event.ind)
		if not N: return True
		
		#find closest jump point to selected location
		x = event.mouseevent.xdata
		y = event.mouseevent.ydata
		distances = np.hypot(x-self.jumps[self.rat][event.ind,0], 
			y-self.jumps[self.rat][event.ind,1])
		m = distances.argmin()
		self.jump_index = int(event.ind[m])	
		if self.jump_index is None:
			return None 

		#draw yellow circle
		self.selected.set_data(self.jumps[self.rat][self.jump_index,0], 
		 self.jumps[self.rat][self.jump_index,1])
		self.selected.set_visible(True)
		self.fig_jumps.canvas.draw()
		
		#update signal data and graph mgram
		self.get_signal_data()
		self.graph_mgram()

	def get_signal_data(self):
		#self.signal_index = self.ji.get_signal_index(self.jump_index) 
		signal_index = self.signal_indices[self.rat][self.jump_index]
		self.signal = self.ji.get_signal(signal_index) 
		self.t, self.s = np.split(self.ji.get_curve(signal_index),[1],1)
		self.jump = self.jumps[self.rat][self.jump_index]
		self.fs = self.ji.get_fs(signal_index)
		
	def graph_mgram(self):
		self.ax_mgram.cla()
	
		#define constants
		nf = 512
		tstep = 30
		tskip = 100
		nf2=nf/2+1 #number of display points
		yconv=int(self.fs/(2*nf2))
		yskip = 10e3/yconv

		#calculate mgram
		mgram = tfr.tfr_spec(self.signal, nf, tstep, nf)
		mgram[mgram==0] = np.amin(mgram[mgram>0])

		#setup labels
		#self.ax_mgram.set_title('Reassinged Spectrogram With Curve Fit')
		self.ax_mgram.set_xlabel('time (s)')
		self.ax_mgram.set_ylabel('frequency (Hz)')
		#self.ax_mgram.set_title('Reassigned Spectrogram of Rat USV')

		#setup time axis coordinates 
		t_axis_coords = np.arange(0, mgram.shape[1] , tskip)
		t_data_coords = np.arange(0, mgram.shape[1]*tstep/self.fs, 
			tstep*tskip/self.fs)
		t_data_coords = np.array([round(elem, 2) for elem in t_data_coords])

		self.ax_mgram.set_xticks(t_axis_coords)
		self.ax_mgram.set_xticklabels(t_data_coords)

		#setup frequency axis coordintes
		f_axis_coords = np.arange(0, nf2, int(yskip))
		f_data_coords = np.arange(0, self.fs/2, yskip*yconv)
		f_data_coords = [format_e(_) for _ in f_data_coords]
		self.ax_mgram.set_yticks(f_axis_coords)
		self.ax_mgram.set_yticklabels(f_data_coords)

		#plot mgram
		im = self.ax_mgram.imshow(np.log(mgram), vmin=np.mean(np.log(mgram)), 
			origin='lower')
		im.set_cmap('Purples')

		"""
		plot jumps and fitted curve in axis (reassinged) coordinates
		the data coordinates are for display only
		"""
		
		self.ax_mgram.plot(self.t*self.fs/tstep, self.s/yconv, color='orange', 
			label='Fitted Curve')
		self.ax_mgram.plot(self.jump[2]*self.fs/tstep, self.jump[0]/yconv, 'o', 
			color='#98E71A', label='Jump Points')
		self.ax_mgram.plot(self.jump[3]*self.fs/tstep, self.jump[1]/yconv, 'o', 
			color='#98E71A')
		
		for line in self.ax_mgram.lines:
			line.set_visible(self.lines_visible)

		"""
		handles, labels = self.ax_mgram.get_legend_handles_labels()
		rect = [0.1, 0.8, 0.1, 0.15] #l,b,w,h
		self.ax_mgram_legend = self.fig_mgram.add_axes(rect)
		self.ax_mgram_legend.get_xaxis().set_visible(False)
		self.ax_mgram_legend.get_yaxis().set_visible(False)
		self.ax_mgram_legend.set_axis_bgcolor((0.75, 0.75, 0.75, 1.0))
		self.ax_mgram_legend.set_frame_on(False)
		self.ax_mgram_legend.legend(handles, labels, markerscale=3)
		"""

		self.fig_mgram.canvas.draw()


#sort jumps first based on cluster then based on distance from the origin	
def sort_jumps(jumps, signal_indices, cluster):
	sorted_jumps = np.zeros(jumps.shape)
	sorted_signal_indices = np.zeros(len(signal_indices))
	distance = np.sqrt(jumps[:,0]**2+jumps[:,1]**2)
	idx = cluster.argsort() 
	cluster = cluster[idx] #cluster is now sorted
	jumps = jumps[idx,:]
	distance = distance[idx]
	signal_indices = signal_indices[idx]  
	for c in np.unique(cluster):
		idx1 = find(cluster == c)
		j = jumps[idx1,:]
		idx2 = distance[idx1].argsort()
		sorted_jumps[idx1,:] = jumps[idx1,:][idx2,:]
		sorted_signal_indices[idx1] = signal_indices[idx1][idx2]
	
	return sorted_jumps, sorted_signal_indices, cluster

if __name__ == '__main__':
	dbPath = './jump.db'
	rat = 'V6'
	ji = jump_interface.JumpInterface(dbPath)
	PointBrowser(rat, ji)